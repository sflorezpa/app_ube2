library(shiny)
library(shinydashboard)
library(dplyr)
library(fst)
library(quanteda)
library(quanteda.textplots)
library(wordcloud)
library(plotly)
library(formattable)

options(shiny.maxRequestSize=30*1024^2) 

# setwd("~/Seguros Suramericana, S.A/Sebastian Florez Parra - dashboard_UB/shiny_ub/")

#data<-read.csv2("bd_parcial_marzo15.csv")
ad<- read.csv2("ay_dx.csv")  

data     <- read.fst("dataset.fst")
dictJPVA <- readRDS("dictJPVA.rds")

shinyServer(function(input, output, session) {
  
  output$selCiudad <- renderUI({
    selectInput("id_sel", c(unique(data$Ciudad),'TODOS'), label = "Seleccione la Ciudad:", selected = 'TODOS')
  })
  
  output$selResul <- renderUI({
    selectInput("id_resul", c(unique(data$InterpretacionResultado),'TODOS'), label = "Seleccione el resultado:", selected = 'TODOS')
  })
 
  
  
  dataCorpus <- function(data) {
    docs_tot <- quanteda::corpus((data$corpus))
    docvars(docs_tot, "NameIDText") <- names(docs_tot)

    docs_tot <- tokens(docs_tot,remove_punct = TRUE,remove_separators = TRUE, remove_symbols = TRUE)
    
  }
  
  output$total_pruebas1 <- renderValueBox({
    valueBox(
      nrow(data), "pruebas realizadas", icon = icon("chart-line", lib = "font-awesome"),
      color = "green"
    )
  })
  
  output$total_pruebas2 <- renderValueBox({
    valueBox(
      round(mean(as.numeric(data$Edad_x), na.rm = T), 2), "es la edad promedio", icon = icon("calendar", lib = "font-awesome"),
      color = "green"
    )
  })
  
  output$total_pruebas3 <- renderValueBox({
    valueBox(
      paste0(round(nrow(filter(data, Sexo == 'F'))/nrow(data), 2)*100, "%"), "son mujeres", icon = icon("venus", lib = "font-awesome"),
      color = "green"
    )
  })
  
  output$tiempo_parati <- renderValueBox({
    
    if (input$id_resul == "TODOS") {
      valueBox(
      paste0(round(nrow(filter(data, Ind_PAT_MAMARIA_TIEMPO_PARATI == '1.0'))/nrow(data), 2)*100, "%"), "están en el programa Tiempo para TI", icon = icon("venus", lib = "font-awesome"),
      color = "green"
        )
    }
    else{
      df=filter(data, InterpretacionResultado == input$id_resul)
      valueBox(
        paste0(round(nrow(filter(df, Ind_PAT_MAMARIA_TIEMPO_PARATI == '1.0'))/nrow(df), 2)*100, "%"), "están en el programa Tiempo para TI", icon = icon("venus", lib = "font-awesome"),
        color = "green"
      )
      
    }
    
  })
  
  output$ay_dx <- renderValueBox({
    
    ad2<-unique(ad[c("Dni_Cliente", "Hora_Servicio", "Servicio_Desc")])
    
    dni=nrow(unique(ad[c("Dni_Cliente")]))
    
    tot=nrow(ad2)/dni
    tot
    
    valueBox(
      round(tot,2), "ayudas dx en promedio por persona", icon = icon("chart-line", lib = "font-awesome"),
      color = "green"
    )
  })
  


  output$grafica_totEXOMAS <- renderPlotly({
    
    mes <- data.frame (Mes = c("ENERO", "FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"),
                       Mes_Num = c("01", "02","03","04","05","06","07","08","09","10","11","12")
    )

    data        <- merge(data, mes, by = 'Mes')
    data$MES    <- paste0(data$Año, data$Mes_Num)
    pruebas_mes <- data.frame(table(data$MES))
    
    names(pruebas_mes)=c("MES", "Total_Exomas")
    
    fig <- plot_ly(pruebas_mes, x=~MES,y = ~Total_Exomas, name = 'Exomas', type = 'scatter', mode = 'lines',
                   line = list(color = 'rgb(0,128,128)', width = 3))
    fig
    
  })
  
  output$grafica_ciudad <- renderPlotly({
    ciudad=data.frame(table(data$Ciudad))
    names(ciudad)=c("Ciudad", "Total_Exomas")
    
    fig <- plot_ly(ciudad,x = ~Total_Exomas, y = ~Ciudad, type = 'bar', orientation = 'h',
                   marker = list(color ="cadetblue"))
    
    fig
  })
  
  output$graficas <- renderPlotly({
    genero=data.frame(table(data$Sexo))
    names(genero)=c("genero", "total")
    f2=plot_ly(genero, labels = ~genero, values = ~total, type = 'pie',
               #textposition = 'inside',
               #textinfo = 'label+percent',
               marker = list(colors = c('#EEE0E5','#7AC5CD'))) # %>% layout(title="Género")
    f2
    
  })
  
  output$grafica_edad <- renderPlotly({
    
    fig <- plot_ly(alpha = 0.6)
    f=data %>% filter(Sexo=="F") %>% select(Edad_x)
    m=data %>% filter(Sexo=="M") %>% select(Edad_x)
    fig <- fig %>% add_histogram(x=f$Edad_x, name="Femenio", color='#EEE0E5')
    fig <- fig %>% add_histogram(x=m$Edad_x, name="Masculino", color='#7AC5CD')
    fig <- fig %>% layout(barmode = "overlay")
    fig
    
  })
  
  output$grafica_rol <- renderPlotly({
    colors <- c('rgb(211,94,96)', 'rgb(128,133,133)', 'rgb(144,103,167)', 'rgb(171,104,87)', 'rgb(114,147,203)')
    rol=data.frame(table(data$Rol_Afiliado_Eps))
    names(rol)=c("Rol", "total")
    f2=plot_ly(rol, labels = ~Rol, values = ~total, type = 'pie',
               #textposition = 'inside',
               textinfo = 'label+percent',
               insidetextfont = list(color = '#FFFFFF'),
               marker = list(colors = colors,
                             line = list(color = '#FFFFFF', width = 0.7)),
               #The 'pull' attribute can also be used to create space between the sectors
               showlegend = FALSE
    )# %>% layout(title="Rol Eps")
    f2
    
  })
  
  output$cloud_word <- renderPlot({
    docs_tot <- dataCorpus(data)
    docs_tot <- tokens_remove(docs_tot, c(stopwords("spanish"), "nsegún","n","recibida", dictJPVA)) %>% dfm()
    set.seed(100)
    textplot_wordcloud(docs_tot, min_freq = 10, random_order = FALSE,
                       rotation = .25,
                       colors = RColorBrewer::brewer.pal(8, "Dark2"))
    
  })
  
  output$frec_term <- renderPlot({
    docs_tot <- dataCorpus(data)
    docs_tot <- tokens_remove(docs_tot, c(stopwords("spanish"), "nsegún","n","recibida", dictJPVA)) %>% dfm()
    barplot(sort(topfeatures(docs_tot,30),decreasing = F),
            col ="lightblue", main ="Palabras más frecuentes: Interpretación Biológica",
            xlab = "Frecuencia de palabras",cex.axis = 1,cex.names=0.5,las=2,
            border = F,horiz = T,ylab="Palabras más frecuentes")
    
  })
  
  output$cloud_word_organo <- renderPlot({
    if (input$id_sel=="TODOS") {
      docs_tot <- dataCorpus(data)
    }
    else{
      data<-filter(data, Ciudad == input$id_sel)
      docs_tot <- dataCorpus(data)
    }
    docs_tot<-tokens_select(docs_tot, c("anal", "colon", "endometrio", "estomago", "ganglios", "hematopoyetico", "intestino", "mama", "medula", "musculo", "ovario", "pancreas", "paratiroides", 
                                        "piel", "prostata", "pulmon", "recto", "renal", "snc", "tiroides", "vejiga", "vesicula", "testiculo", "higado",
                                        "suprarenal"))%>% 
      dfm()
    set.seed(100)
    textplot_wordcloud(docs_tot, min_freq = 1, random_order = FALSE,
                       rotation = .25,
                       colors = RColorBrewer::brewer.pal(8, "Dark2"),
                       min_size = 2,
                       max_size = 10)
    
  })
  
  
  output$resultado_ciudad <- renderPlotly({
    
    if (input$id_sel == "TODOS") {
      df=data
      resultado=data.frame(table(df$InterpretacionResultado))
      names(resultado)=c("Resultado", "Total")
      plot_ly(resultado, labels = ~Resultado, values = ~Total, type = 'pie')
    } 
    else{
      df=filter(data, Ciudad == input$id_sel)
      resultado=data.frame(table(df$InterpretacionResultado))
      names(resultado)=c("Resultado", "Total")

      plot_ly(resultado, labels = ~Resultado, values = ~Total, type = 'pie')
             
    }

  })
  
  
  output$consaguinidad <- renderPlotly({
    
    if (input$id_sel == "TODOS") {
      sums=colSums(data[ , c("primergrado", "segundogrado", "tercergrado")])
      sums=as.data.frame(sums)
      sums <- cbind(consaguinidad = rownames(sums), sums)
      p1 <- plot_ly(x = sums$consaguinidad,
                    y = sums$sums,
                    name = "Cities",
                    type = "bar")
      p1
      
    } 
    else{
      df=filter(data, Ciudad == input$id_sel)
      sums=colSums(df[ , c("primergrado", "segundogrado", "tercergrado")])
      sums=as.data.frame(sums)
      sums <- cbind(consaguinidad = rownames(sums), sums)
      p1 <- plot_ly(x = sums$consaguinidad,
                    y = sums$sums,
                    name = "Cities",
                    type = "bar")
      p1
      
    }
    
  })
  
  output$resultado_marcas <- renderPlotly({
    
    if (input$id_resul == "TODOS") {
      df=data
      cols.num <- c("Ind_CANCER", "Ind_CRONICO", "Ind_DIABETES", "Ind_HTA", 
                    "Ind_PAT_MAMARIA_TIEMPO_PARATI", "Ind_PROGRAMA_CUIDADO_PALIATIVO")
      df[cols.num] <- sapply(df[cols.num],as.numeric)
  
      
      sums=colSums(df[ , c("Ind_CANCER", "Ind_CRONICO", "Ind_DIABETES", "Ind_HTA", 
                           "Ind_PAT_MAMARIA_TIEMPO_PARATI", "Ind_PROGRAMA_CUIDADO_PALIATIVO")], na.rm = T)
      sums=as.data.frame(sums)
      sums <- cbind(consaguinidad = rownames(sums), sums)
      p1 <- plot_ly(x = sums$consaguinidad,
                    y = sums$sums,
                    name = "Cities",
                    type = "bar")
      p1
      
    } 
    else{
      
      df=filter(data, InterpretacionResultado == input$id_resul)
      cols.num <- c("Ind_CANCER", "Ind_CRONICO", "Ind_DIABETES", "Ind_HTA", 
                    "Ind_PAT_MAMARIA_TIEMPO_PARATI", "Ind_PROGRAMA_CUIDADO_PALIATIVO")
      df[cols.num] <- sapply(df[cols.num],as.numeric)
      
      
      sums=colSums(df[ , c("Ind_CANCER", "Ind_CRONICO", "Ind_DIABETES", "Ind_HTA", 
                           "Ind_PAT_MAMARIA_TIEMPO_PARATI", "Ind_PROGRAMA_CUIDADO_PALIATIVO")], na.rm = T)
      sums=as.data.frame(sums)
      sums <- cbind(consaguinidad = rownames(sums), sums)
      p1 <- plot_ly(x = sums$consaguinidad,
                    y = sums$sums,
                    name = "Cities",
                    type = "bar")
      p1
      
    }
    
  })
  
  
  
  output$ayudas_dx <- renderFormattable({
    ad2<-unique(ad[c("Dni_Cliente", "Hora_Servicio", "Servicio_Desc")])
    
    ayud=table(ad2$Servicio_Desc) %>% 
      as.data.frame() %>% 
      arrange(desc(Freq)) %>%  head(15)
    names(ayud)=c("Ay_DX", "total")
    
    
    formattable(ayud)
    
 
    
  })
  
  
  

})















